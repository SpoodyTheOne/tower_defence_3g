static class EconomyManager {
  // 50 starting money to allow for money towers
  static int currency = 100;

  static void add_currency(int amount) {
    currency += amount;
  }

  static boolean can_afford(int price) {
    return currency >= price;
  }

  static void spend(int amount) {
    currency -= amount;
  }
}
