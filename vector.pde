class Vector<T> {
  public Vector(int capacity) {
    this.capacity = capacity;
    // Java arrays are dynamically checked (why) so we need this hack to work around it
    this.buffer = (T[]) new Object[this.capacity];
  }

  public Vector() {
    this.capacity = 1;
    // Java arrays are dynamically checked (why) so we need this hack to work around it
    // Also code duplication because you cant call another constructor method from a constructor
    // method
    this.buffer = (T[]) new Object[this.capacity];
  }

  public int len() {
    return this.length;
  }

  public void push(T value) {
    // Double size of buffer if a push is attempted that would go out of bounds
    if (this.length + 1 >= this.capacity) {
      T[] arr = (T[]) new Object[this.capacity*2];
      System.arraycopy(this.buffer, 0, arr, 0, this.length);
      this.buffer = arr;
      this.capacity *= 2;

      if (DEBUG) {
        println("Vector realloc");
      }
    }

    this.buffer[this.length] = value;
    this.length++;
  }

  public T get(int index) {
    this.errorOutsideBounds(index);

    return this.buffer[index];
  }

  // Does the same as get, except it doesnt check bounds.
  // Should be slightly more optimized, but can lead to
  // undefined behavior if used without care
  public T get_unchecked(int index) {
    return this.buffer[index];
  }

  public T remove(int index) {
    this.errorOutsideBounds(index);

    // Restructure the array so it no longer
    // contains the value at index
    //
    // We do this by using System.arraycopy() to copy the 
    // indeces after the removed element on top, and then     
    // decrementing the length.
    //
    // The duplicate element will not be accesible due to the bounds check
    //
    // [ (), (), (), (), R, (), (), () ]
    //                ||
    //                || System.arraycopy(...)
    //                \/
    // [ (), (), (), (), (), (), (), Duplicate ]
    //                ||
    //                || this.length--
    //                \/
    // [ (), (), (), (), (), (), () ]

    // If the element is at the back we can simply remove it
    if (index == this.length - 1) {
      this.length--;
      return this.buffer[index];
    }
    
    // Otherwise, do the steps detailed above
    System.arraycopy(this.buffer, index + 1, this.buffer, index, this.length - (index + 1));
    this.length--;
    return this.buffer[index];
  }

  private void errorOutsideBounds(int index) {
    if (index >= this.length || index < 0) {
      IndexOutOfBoundsException ex = new IndexOutOfBoundsException("Index " + index + " is out of bounds for vector with size " + this.length);
      ex.printStackTrace();
      throw ex;
    }
  }

  private T[] buffer;
  private int length;
  private int capacity;
}
