class MoneyTower extends Tower {
  MoneyTower() {
    this.shootDelay = 8500*2;
    this.lastShoot = millis() + this.shootDelay;
  }
  
  void draw(float x) {
    int diff = (millis() - this.lastShoot)/5;
    fill(255 + diff, 255 + diff, 255);
    rect(x + LANE_TILE_WIDTH/2, 20, 30, LANE_TILE_HEIGHT-49);
  }

  Projectile projectile(float progress) {
    return null;
  }

  void tick(float x, boolean hasEnemies) {
    if (millis() > this.lastShoot) {
      this.lastShoot = millis() + this.shootDelay;

      EconomyManager.add_currency(25);
    }
  }
}
