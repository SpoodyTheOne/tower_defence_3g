abstract class Enemy {
  float progress = 0f;
  float health = 100f;
  float speed = 0.3f;

  float time_since_hit = 0;

  private Enemy() {
    this.speed *= random(1.0f,1.5f);
  }
  
  void tick() {
    if (!this.isDead())
      this.progress += this.speed * deltaTime;
  }
  
  void draw() {
    tint(255, 255 - time_since_hit, 255 - time_since_hit);
    time_since_hit -= 3*deltaTime;
  };

  float getProgress() {
    return LANE_TILE_WIDTH*LANE_LENGTH - this.progress;
  }

  void damage(float amount) {
    this.health -= amount;
    this.time_since_hit = 255;
    if (this.health <= 0) {
      this.kill();
    }
  }

  boolean isDead() {
    return this.health <= 0f;
  }
  
  abstract void kill();
}
