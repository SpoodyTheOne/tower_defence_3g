class BasicTower extends Tower {
  BasicTower() {
    tower_basic_img.resize(80, int(LANE_TILE_HEIGHT-30));
  }
  
  void draw(float x) {
    fill(#e5bc42);
    tint(255);
    // rect(x + LANE_TILE_WIDTH/2, 20, 30, LANE_TILE_HEIGHT-49);
    image(tower_basic_img, x + 40, 10, 80, LANE_TILE_HEIGHT-30);
  }

  Projectile projectile(float progress) {
    return new BasicProjectile(progress);
  }

  void tick(float x, boolean hasEnemies) {
    super.tick(x + 90, hasEnemies);
  }
}

class BasicProjectile extends Projectile {
  BasicProjectile(float progress) {
    this.damage = 6;
    this.progress = progress;
  }

  void draw() {
    super.draw();
  }
}
