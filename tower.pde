abstract class Tower {
  float health = 100f;
  Vector<Projectile> projectiles = new Vector<Projectile>(3);
  int lastShoot = 0;
  int shootDelay = 1800;

  abstract void draw(float x);

  protected abstract Projectile projectile(float progress);

  void tick(float x, boolean hasEnemies) {
    if (hasEnemies && millis() > this.lastShoot) {
      this.lastShoot = millis() + this.shootDelay;

      this.projectiles.push(this.projectile(x));
    }
  }
}

abstract class Projectile {
  float progress = 0f;
  float speed = 3.2f;
  float damage = 0f;

  void draw() {
    ellipse(this.progress, LANE_TILE_HEIGHT/2, 40,40);
  }

  void tick() {
    this.progress += this.speed * deltaTime;
  }
}

class TowerData {
  String name;
  String description;
  int cost;
  int lastPlaced = 0;
  int recharge_time;

  int index = 0;

  TowerData(String name, int cost, int recharge, int index) {
    this.name = name;
    this.cost = cost;
    this.recharge_time = recharge;

    this.index = index;
  }

  boolean canPlace() {
    return millis() > this.lastPlaced + this.recharge_time;
  }

  String seconds_to_place() {
    int ms = (this.lastPlaced + this.recharge_time) - millis();

    if (ms <= 0) 
      return "0s";
    
    return int(ceil((float)ms)/1000) + "s";
  }
  
  Tower toTower() {
    this.lastPlaced = millis();
    return towerFactory(this.index);
  }
}

class TowerManager {
  TowerManager() {
    this.Towers = new Vector<TowerData>();

    int index = 0;
    
    this.Towers.push(new TowerData("Pee Shooter", 100, 10000, index++));
    this.Towers.push(new TowerData("Suck flower", 50, 10000, index++));
    this.Towers.push(new TowerData("Shit mine", 25, 30000, index++));

    this.Towers.get(1).lastPlaced = -99999;
  }
  
  Vector<TowerData> Towers;
}

Tower towerFactory(int selected) {
  switch (selected) {
    case 0:
      return new BasicTower();
    case 1:
      return new MoneyTower();

    case 2:
      return new Landmine();

    default:
      return null;
  }
}
