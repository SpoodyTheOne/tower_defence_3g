class Landmine extends Tower {
  Landmine() {
    tower_basic_img.resize(80, int(LANE_TILE_HEIGHT-30));
    this.shootDelay = 99999999;
    this.lastShoot = millis() + 10000;
  }

  boolean has_shot = false;
  
  void draw(float x) {
    fill(#e5bc42);
    tint(255);
    // rect(x + LANE_TILE_WIDTH/2, 20, 30, LANE_TILE_HEIGHT-49);
    image(this.has_shot ? tower_landmine_armed : tower_landmine_unarmed, x + 40, 10, 80, LANE_TILE_HEIGHT-30);
  }

  Projectile projectile(float progress) {
    this.has_shot = true;
    return new LandmineProjectile(progress);
  }

  void tick(float x, boolean hasEnemies) {
    super.tick(x + 90, hasEnemies);

    if (this.has_shot && this.projectiles.len() == 0) {
      this.health = 0;
    }
  }
}

class LandmineProjectile extends Projectile {
  LandmineProjectile(float progress) {
    this.damage = 500;
    this.progress = progress;
    this.speed = 0.0f;
  }

  void draw() {
    // super.draw();
  }
}
