boolean DEBUG = false;
color DEBUG_COLOR = #f402b8;
int DEBUG_TEXT_SIZE = 10;

// Setting the initial capacity to the amount we want 
// removes unecessary array copies
Vector<Lane> lanes = new Vector<Lane>(5);

// Extra space from the top and bottom of the screen in pixels
int LANE_MARGIN = 0;
// Space between lanes in pixels
int LANE_SPACING = 0;

// How far each lane gets translated in the draw method.
// exposed so the function for calculating which tile you are
// hovering over can also use it
float LANE_DRAW_TRANSLATION = 0f;

// Grace period before any enemies can spawn
int LEVEL_START_GRACE = 20000;
int LEVEL_START_MILLIS = 0;

float deltaTime = 0.0f;
float lastMillis = 0;

int selectedTower = -1;
Tower selectedTowerInstance = null;
boolean pMousePressed = false;

// Enemy sprites
PImage basic_img;
PImage advanced_img;

// Tower sprites
PImage tower_basic_img;
PImage tower_landmine_armed;
PImage tower_landmine_unarmed;

TowerManager towerManager;

void setup() {
  fullScreen(1);
  // size(600,400);
  noStroke();
  frameRate(144);

  towerManager = new TowerManager();

  // Load sprites
  // Enemies
  basic_img = loadImage("zombie.png");
  advanced_img = loadImage("zombie_advanced.png");
  // Tower sprites
  tower_basic_img = loadImage("boy.png");
  tower_landmine_armed = loadImage("Landmine_armed.png");
  tower_landmine_unarmed = loadImage("Landmine_unarmed.png");

  // Create 5 lanes
  lanes.push(new Lane(0));
  lanes.push(new Lane(1));
  lanes.push(new Lane(2));
  lanes.push(new Lane(3));
  lanes.push(new Lane(4));
  
  LANE_MARGIN = 200;
  LANE_SPACING = 20;
  
  LANE_TILE_HEIGHT = (height - LANE_MARGIN - LANE_SPACING*lanes.len() )/lanes.len();
  LANE_TILE_WIDTH = (width - LANE_MARGIN)/LANE_LENGTH;

  println("lane height: " + LANE_TILE_HEIGHT);
}

void draw() {
  // Calculate deltatime;
  deltaTime = (((int)millis()) - lastMillis)/10;
  lastMillis = millis();

  background(0);
  
  // Save the current matrix
  pushMatrix();

  LANE_DRAW_TRANSLATION = (height-LANE_MARGIN/2)/lanes.len();
  
  translate(width - LANE_TILE_WIDTH*LANE_LENGTH,-LANE_DRAW_TRANSLATION);

  int hover_lane = mouse_hover_lane();
  int hover_column = mouse_hover_column();

  for (int i = 0; i < lanes.len(); i++) {
    // Get unchecked as we will always be within the bounds of the vector
    Lane lane = lanes.get_unchecked(i);

    // Translate origin further down
    translate(0, LANE_DRAW_TRANSLATION + LANE_SPACING);

    // Draw lane
    lane.draw();

    // Tick lane. This includes all enemies and towers on said lane
    lane.tick();

    // Check if the mouse is currently over the current lane
    if (i == hover_lane && hover_column >= 0) {

      // Check if the mouse has been pressed this frame
      if (!pMousePressed && mousePressed) {

        // Figure out which mouse button was pressed
        switch (mouseButton) {
          case LEFT: // Left mouse button means placing a building.
            if (selectedTowerInstance != null) { // If selectedTowerInstance is null we dont have a building in our hand
              // Get data relavent to the current tower
              TowerData towerData = towerManager.Towers.get(selectedTower);

              // If we cant place it due to cooldowns we skip to the next iteration in the loop
              if (!towerData.canPlace())
                continue;

              // Check if we can afford to place the tower
              if (EconomyManager.can_afford(towerData.cost)) {
                lane.place_tower(hover_column, towerData.toTower());    // Add the tower to the lane
                EconomyManager.spend(towerData.cost);                   // Remove the money from the wallet
                selectedTower = -1;                                     // Deselect the current tower
                selectedTowerInstance = null;                           // -||-
              }
            }
            break;

          case RIGHT: // Right mouse is for removing towers on the current tile
            lane.remove_tower(hover_column);
            break;

          case CENTER: // Middle mouse button to toggle debug overlay
            DEBUG = !DEBUG;
            break;

            // Default case which does nothing
          default:
            break;
        }
      }

      fill(255,100);

      rect(hover_column * LANE_TILE_WIDTH, 0, LANE_TILE_WIDTH, LANE_TILE_HEIGHT);
    }
    
  }

  // If there are no enemies on screen and we are not within the grace period we start the next wave
  if (TOTAL_ENEMIES == 0 && millis()-LEVEL_START_MILLIS > LEVEL_START_GRACE) {
    nextWave();
  }

  // Pop matrix after we modified it from drawing lanes
  popMatrix();

  // Draw the selected tower on the cursor
  pushMatrix();

  if (selectedTowerInstance != null) {
    // Translate the origin to be on the cursor
    translate(mouseX - LANE_TILE_WIDTH/2, mouseY - LANE_TILE_HEIGHT/2);
    selectedTowerInstance.draw(0);
  }

  // Restore to default matrix
  popMatrix();

  // Draw the current money in wallet
  textSize(25);
  textAlign(CENTER);
  fill(255);
  text("" + EconomyManager.currency, 50, 80);
  textAlign(LEFT);

  // If we have a tower selected we also draw the TowerData for that tower
  if (selectedTower != -1) {
    TowerData towerData = towerManager.Towers.get(selectedTower);

    text(towerData.name, 10, 120);
    text("cost: " + towerData.cost, 10, 150);
    text("Available in: " + towerData.seconds_to_place(), 10, 180);
  }

  // Debug information
  if (DEBUG) {
    fill(DEBUG_COLOR);
    textSize(DEBUG_TEXT_SIZE);
    text("Enemies: " + TOTAL_ENEMIES, 5, 20);
    text("fps: " + (int)ceil(frameRate), 5, 30);
    text("selectedTower: " + selectedTower, 5, 40);
  }

  pMousePressed = mousePressed;
}

// Helper function for getting the current lane the mouse is over
int mouse_hover_lane() {
  int index = (int)(mouseY / (LANE_DRAW_TRANSLATION + LANE_SPACING));
  
  // println("mouse over: " + index);
  
  return index; 
}

// Helper function for getting the current column in the lane the mouse is over
int mouse_hover_column() {
  float index = (mouseX - (width - LANE_TILE_WIDTH*LANE_LENGTH)) / LANE_TILE_WIDTH;

  if (index < 0)
    return -1;
  
  return (int)index;
}

// When the mouse is scrolled we change which tower is in the hand
void mouseWheel(MouseEvent event) {
  float e = event.getCount();

  selectedTower += e;

  if (selectedTower < 0)
    selectedTower = towerManager.Towers.len() - 1;

  selectedTower = selectedTower % towerManager.Towers.len();

  selectedTowerInstance = towerFactory(selectedTower);
}

// If we press escape we remove the tower from the hand
void keyPressed() {
  if (keyCode == 27) {
    selectedTower = -1;
    selectedTowerInstance = null;
  }
}

// Override the default exit method to remove esc to close
void exit() {
  // No more esc to close
  if (keyCode != 27) {
    super.exit();
  }
}
