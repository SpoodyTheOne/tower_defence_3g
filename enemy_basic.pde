class BasicEnemy extends Enemy {
  BasicEnemy() {
    super();
    basic_img.resize(80, int(LANE_TILE_HEIGHT-20));
  }

  void draw() {
    super.draw();
    image(basic_img, getProgress(), 0, 80, LANE_TILE_HEIGHT-20);
  }
 
  void kill() {
    // TODO
  }
}


class AdvancedEnemy extends Enemy {
  AdvancedEnemy() {
    super();
    this.health = 250f;
  }

  void draw() {
    super.draw();
    image(this.health > 100 ? advanced_img : basic_img, getProgress(), 0, 80, LANE_TILE_HEIGHT-20);
  }
 
  void kill() {
    // TODO
  }
}
