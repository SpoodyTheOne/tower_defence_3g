color LANE_COLOR = #4fc160;
color LANE_ALT_COLOR = #3aa34a;
final int LANE_LENGTH = 12;
float LANE_TILE_WIDTH = 0f;
float LANE_TILE_HEIGHT = 0f;

// How long the cleanup waves take in ms.
// Cleanup waves remove dead enemies from the enemies array
int LANE_CLEANUP_TIMER = 1000;

int TOTAL_ENEMIES = 0;

class Lane {
  Lane(int _offset) {
    this.offset = _offset;

    // Start with capacity for 5 enemies per lane
    this.enemies = new Vector<Enemy>(5);
  }

  int offset;

  Tower[] towers = new Tower[LANE_LENGTH];

  Vector<Enemy> enemies;
  int lastCleanup = 0;
  float enemyFurthestProgress = 0f;
  int furthestEnemy = -1;

  public void draw() {
    // Draw lanes
    for (int i = 0; i < LANE_LENGTH; i++) {
      // Changes color between each square
      // Offset makes sure lanes positioned on top of
      // eachother alternate differently. This creates
      // a chess board effect rather than vertical lines
      fill((i + this.offset) % 2 == 0 ? LANE_COLOR : LANE_ALT_COLOR);
      // fill(#FF0000);

      rect(LANE_TILE_WIDTH*i, 0, LANE_TILE_WIDTH, LANE_TILE_HEIGHT);
    }
    
    // Draw towers
    for (int i = 0; i < LANE_LENGTH; i++) {
      Tower tower = this.towers[i];
      if (tower == null)
        continue;

      tower.draw(LANE_TILE_WIDTH*i);

      tower.tick(LANE_TILE_WIDTH*i, this.furthestEnemy != -1);

      if (tower.health == 0) {
        this.towers[i] = null;
        continue;
      }

      Vector<Integer> toBeRemoved = new Vector<Integer>(tower.projectiles.len());
      
      for (int j = 0; j < tower.projectiles.len(); j++) {
        Projectile proj = tower.projectiles.get(j);

        proj.tick();
        proj.draw();

        // Remove projectiles that have flown off the screen
        if (proj.progress > width) {
          toBeRemoved.push(j);
          continue;
        }

        if (this.furthestEnemy != -1 && proj.progress > this.enemyFurthestProgress && proj.progress < this.enemyFurthestProgress + 50) {
          if (this.enemies.len() <= this.furthestEnemy) {
            this.furthestEnemy = -1;
            continue;
          }

          this.enemies.get(this.furthestEnemy).damage(proj.damage);
          toBeRemoved.push(j);
        }
      }

      if (toBeRemoved.len() > 0)
        println(toBeRemoved.buffer);

      for (int j = toBeRemoved.len()-1; j >= 0; j--) {
        tower.projectiles.remove(toBeRemoved.get(j));
      }
    }


    if (this.enemies.len() == 0) 
      this.furthestEnemy = -1;

    this.enemyFurthestProgress = 9999999;
    
    // Draw enemies
    for (int i = 0; i < this.enemies.len(); i++) {
      Enemy enemy = this.enemies.get(i);
      enemy.tick();
      enemy.draw();

      float progress = enemy.getProgress();

      // The enemy has reached the end of the lawn
      if (progress < 0) {
        exit();
      } else if (progress < this.enemyFurthestProgress) {
        this.enemyFurthestProgress = progress;
        this.furthestEnemy = i;
      }
    }

    if (DEBUG) {
      stroke(DEBUG_COLOR);
      fill(DEBUG_COLOR);
      textSize(DEBUG_TEXT_SIZE);
      strokeWeight(3);
      line(0,LANE_TILE_HEIGHT/2, LANE_TILE_WIDTH*LANE_LENGTH,LANE_TILE_HEIGHT/2);
      // textAlign(RIGHT);
      text("furthestEnemy: " + this.furthestEnemy, 5, 15);
      text("enemyFurthestProgress: " + this.enemyFurthestProgress, 5, 25);
      // textAlign(LEFT);
      noStroke();
    }
  }

  public void tick() {
    // Clean up dead enemies from the enemy vector every so often
    if (millis() > this.lastCleanup) {
      this.lastCleanup = millis() + LANE_CLEANUP_TIMER;

      Vector<Integer> toBeRemoved = new Vector<Integer>(this.enemies.len());

      for (int i = 0; i < this.enemies.len(); i++) {
        if (this.enemies.get(i).isDead()) {
          toBeRemoved.push(i);
        }
      }

      int len = toBeRemoved.len();

      for (int i = 0; i < len; i++) {
        this.enemies.remove(toBeRemoved.get(i));
      }

      TOTAL_ENEMIES -= len;

      if (DEBUG && len > 0) {
        println("Cleaned up " + len + " dead enemies");
      }
    }
  }

  public void add_enemy(Enemy enemy) {
    this.enemies.push(enemy);
    TOTAL_ENEMIES++;
  }

  public boolean place_tower(int position, Tower tower) {
    if (this.towers[position] != null)
      return false;

    this.towers[position] = tower;
    return true;
  }

  public boolean remove_tower(int position) {
    if (this.towers[position] == null)
      return false;

    this.towers[position] = null;
    return true;
  }
}
