int totalWaves = 0;

void nextWave() {
  for (int i = 0; i < totalWaves/2; i++) {
    Lane lane = lanes.get((int)random(0,lanes.len()));

    Enemy enemy = random_enemy_weighted(totalWaves);

    enemy.progress -= random(0,width/25);
    
    lane.add_enemy(enemy);
  }

  totalWaves++;
}

Enemy random_enemy_weighted(int max) {
  int num = floor(sqrt(random(0,max)));

  switch (num) {
    case 2:
      return new AdvancedEnemy();
    default:
      return new BasicEnemy();
  }
}
